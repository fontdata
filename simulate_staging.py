#!/usr/bin/python3
# Copyright 2019 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Copies files from checkout dir to staging dir based on contents.json.

Invoke from the checkout directory (the directory that contains .jiri_root).

Example:
   ./fontdata/simulate_staging.py $HOME/tmp/staging-dir

Once done, the directory contains the staged files ready for upload.
"""

import argparse
import json
import shutil
from pathlib import Path


def main():
  parser = argparse.ArgumentParser(
      description=(
          'Copy files from checkout directory to staging directory using '
          'instructions in <checkout-dir>/contents.json'))
  parser.add_argument('staging_dir', metavar='staging-dir', type=str)
  parser.add_argument('--checkout-dir', type=str, default='.',
                      help='Path to jiri checkout directory. Defaults to current working directory.')
  parser.add_argument('--contents-path', type=str, default='fontdata',
                      help='directory path to contents.json, relative to checkout dir')
  args = parser.parse_args()

  staging_dir = Path(args.staging_dir).resolve()
  checkout_dir = Path(args.checkout_dir).resolve()
  contents_path = checkout_dir.joinpath(args.contents_path, 'contents.json')
  with open(contents_path, 'r') as file:
    contents = json.load(file)
    for bundle in contents:
      destination_dir = staging_dir.joinpath((bundle['destination']))
      destination_dir.mkdir(parents=True, exist_ok=True)
      for file_path in bundle['files']:
        file_name = Path(file_path).name
        src = checkout_dir.joinpath(file_path)
        dst = destination_dir.joinpath(file_name)
        shutil.copy(src, dst)


if __name__ == '__main__':
  main()
