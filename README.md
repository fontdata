# Font Data

This repository hosts JSON files containing metadata for open-source fonts that may be distributed
as part of Fuchsia. The metadata allows CIPD packages, GN packages, and manifests to be generated
for fonts.

**No actual font files (TTF, OTF, WOFF, etc.) should ever be checked into this repo.**

TODO(kpozin): Add documentation link for overview of fonts on Fuchsia.